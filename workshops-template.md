## @ Mention facilitator

We're looking for volunteer workshop leaders that are subject matter experts and we thought of you for this workshop.

We will provide:
* AV setup (screen, projector, mic)
* Support in connecting to helpful team members (e.g. help build out a slide deck, write an abstract, help rehearse etc)
* Contact with the hotel for any special requests

We're looking to you for:
* 45 min workshop program
* 15 min time for Q&A
* We recommend adding some coaches - you don't need to do this alone.

Please nominate another volunteer leader if you don't feel comfortable leading the workshop, but know that we choose you as we believe you're a great teacher on this topic.

Let us know how we can help with making arrangements in this issue and we'll make it a great workshop together.

If you can do this, let us know. Below are a few deadlines for the content:

* Feb 22nd - Confirmation to lead the workshop
* March 11th - First draft for the workshop
* April 1st - Final version for workshop