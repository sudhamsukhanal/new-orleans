# A GitLabbers' guide to extending your stay in NOLA!

If you're planning on doing some sightseeing in NOLA before or after Contribute 
2019, here are some recommendations

## Places to eat

- **Parkway Bakery** http://parkwaypoorboys.com/  

The best place for po' boys (apparently!)

- **Elizabeth's** http://www.elizabethsrestaurantnola.com/

This is a little out of the way (I walked from the French Quarter - about 40 min
but you get to see some really quaint houses along the way), but definitely worth
a visit for the decor and praline bacon! You can walk back through Crescent Park along the river.

- **Company Burger** https://www.thecompanyburger.com/

Informal burger place with a mayo bar! Walkable from the French Quarter.

- **Cafe Beignet** http://www.cafebeignet.com/

Beignets with live jazz. Apparently the beignets here are better than the famous 
Cafe du Monde but we didn't make it to the latter to compare!

- **Herbsaint** https://herbsaint.com/ 

Upmarket restaurant serving modern southern food. If they have the beef shortrib
starter with the potato rosti you have to get it! They had pretty good gumbo too.

- **Three Muses** http://www.3musesnola.com/

Dinner with live music - we saw Washboard Rodeo who were great! They don't charge
admission so remember to take cash to tip the band. The food and cocktails were great.

- **St Roch Market** https://www.neworleans.strochmarket.com/

Indoor street food market with interesting food.

- **French Truck Coffee** https://frenchtruckcoffee.com/

Get the NOLA iced coffee.

## Things to do

- **Steamboat Natchez** http://www.steamboatnatchez.com/ 

A ride along the Mississippi in an old steam boat. A little gimmicky but we really
enjoyed it. Don't bother with the food, I read a lot of reviews saying it's not great.

- **St Louis Cemetery No 1**

You can't go in on your own anymore so you have to book a tour. We went with [these guys](https://www.frenchquarterphantoms.com/)
and had a really entertaining guide. Some people think a cemetery tour is a bit morbid but we loved it! 

- **Frenchmen Street**

Great for live music (this is where 3 Muses is) and a little less touristy than 
Bourbon Street. There was a brass band just playing on the corner when we were there on a Monday night :D

- **Garden District**

Get a streetcar down here and do a self-guided tour. You can go into Lafayette 
Cemetery No 1 on your own and check out some really grand mansions in the area. 

- **Pharmacy Museum** http://www.pharmacymuseum.org/

This is one of the best things I did in NOLA. It only costs $5. Go for the tour 
at 1pm (there's only one a day) as it doesn't cost anything extra and was so interesting! 
The tour guide is really funny, take a few dollars to tip him.

- **Voodoo Museum** http://www.voodoomuseum.com/

This was a little random - it's very small and is more like a shrine than a museum, 
but worth popping in if you're into voodoo history at all. 

- The **Southern Food & Beverage Museum** is also supposed to be awesome but didn't get there. https://natfab.org/southern-food-and-beverage/

Some other restaurants that were on my list but didn't make it to:

- http://alwayssmokin.com/ (One of our uber drivers said this place does the best crawfish boil in NOLA!)
- http://tableaufrenchquarter.com/ (our airbnb host recommended this) 
- http://www.lapetitegrocery.com/
- http://www.shayarestaurant.com/
- http://www.pagodacafe.net/
- http://centralgrocerynola.com/ 
- https://www.commanderspalace.com/
- http://emerilsrestaurants.com/emerils-delmonico
- https://cochonrestaurant.com/ 
- http://www.atchafalayarestaurant.com/
- https://www.deanies.com/ 

You are also welcome to check out and duplicate my [NOLA Google Map](https://drive.google.com/open?id=12ywDK2gUWrHwYIiTQ2tGZCVVBZY&usp=sharing).
