### Introducing...

As we are getting ready for our team and guests to travel to New Orleans in May, we would like to introduce to you, the GitLab Contribute Ambassadors.

### Who are they?

| Name | Department | E-mail | Slack |
| ------ | ------ | ------ | ------ |
| Lisa     | Sales       | lisa@       | [Lisa](https://slack.com/app_redirect?channel=@lvandekooij) |
| Aricka   | Marketing   | aflowers@   | [Aricka](https://slack.com/app_redirect?channel=@aflowers) |
| John     | Engineering | john@       | [Northrup](https://slack.com/app_redirect?channel=@northrup) |
| Suri     | Marketing   | spatel@     | [Suri](https://slack.com/app_redirect?channel=@spatel) |
| Brendan  | Product     | boleary@    | [Brendan](https://slack.com/app_redirect?channel=@boleary) |
| Marin    | Engineering | marin@      | [Marin](https://slack.com/app_redirect?channel=@marin) |
| Luke     | Marketing   | luke@       | [Luke](https://slack.com/app_redirect?channel=@luke) |
| Lyle     | Support     | lyle@       | [Lyle](https://slack.com/app_redirect?channel=@lkozloff) |
| Lee      | Support     | lee@        | [Lee](https://slack.com/app_redirect?channel=@lee) |
| Adam O.  | Sales       | aolson@     | [Adam Olson](https://slack.com/app_redirect?channel=@adamolson) |
| Rebecca  | Marketing   | rebecca@    | [Rebecca](https://slack.com/app_redirect?channel=@rebecca) |
| Nadia    | People      | nadia@      | [Nadia Vatalidis](https://slack.com/app_redirect?channel=@nadiav) |

### What are they doing?

They will be your first point of contact for any questions. 
First, try to find the answer to your question in our documentation.
If you can't find what you're looking for, you can contact them over email. They can help or will escalate the question to the planning team if required.

### Onsite

When everyone has arrived safe and sound, they will assist us with a lot of things while we are enjoying ourselves in New Orleans. 
They will assist with logistics, program information or special requests for example.


