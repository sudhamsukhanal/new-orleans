### [GitLab Contribute 2019](https://about.gitlab.com/company/culture/contribute/) (< link)

#### Questions?

**In order of priority** to get answers:
1. All details are mentioned on this project page.
1. Have your tried the different [files with information?](https://gitlab.com/gitlabcontribute/new-orleans/tree/master)
1. or the [FAQ issue?](https://gitlab.com/gitlabcontribute/new-orleans/issues/20)
1. If your answer wasn't there, did you reach out to your team's [Contribute Ambassador?](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/contribute-ambassadors.md)
1. If not one of those 4 steps answer your question, please [email the Contribute team](mailto:contribute@gitlab.com)

#### When:
May 8th-14th 2019 in New Orleans

> Hyatt Regency Downtown
>
> 601 Loyola Avenue
>
> New Orleans, Louisiana, United States, 70113


#### [Booking flights - READ carefully!](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/team-travel.md) (< link)
Details to booking flights are here: [please read this page carefully!

#### [Registration (and ticket info for guests)](https://gitlab.com/gitlabcontribute/new-orleans/issues/59) (< link)

#### [Overall Schedule, clickable per day](http://www.cvent.com/events/gitlab-contribute-new-orleans/agenda-67789ab7ebee4203995d24c5b183c069.aspx)

#### [Packing and Travel Logistics](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/packing-list.md)

#### Contribute App

You can find the Contribute app by searching "AttendeeHub by Cvent" in your app store of choice or scan the QR code below for:

**APPLE APPSTORE**

![Itunes](images/Itunes.png)

**GOOGLE PLAY**

![Android](images/Android.png)
